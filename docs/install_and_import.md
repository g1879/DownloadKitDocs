---
hide:
  - navigation
---
<script src="https://cdn.wwads.cn/js/makemoney.js" async></script>
<div class="wwads-cn wwads-horizontal" data-id="317"></div><br/>

## 🎟️ 安装

如果您安装过 DrissionPage，本库已自动安装。

```shell
pip install DownloadKit
```

---

## 🎫 导入

```python
from DownloadKit import DownloadKit
```