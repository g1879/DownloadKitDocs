---
hide:
  - navigation
---

<script src="https://cdn.wwads.cn/js/makemoney.js" async></script>
<div class="wwads-cn wwads-horizontal" data-id="317"></div><br/>

这里是本库作者的其它作品，欢迎关注。

## ✅️ DrissionPage

基于python的网页自动化工具。既能控制浏览器，也能收发数据包。可兼顾浏览器自动化的便利性和requests的高效率。  

功能强大，内置无数人性化设计和便捷功能。语法简洁而优雅，代码量少。

**项目地址：**[https://gitee.com/g1879/DrissionPage](https://gitee.com/g1879/DrissionPage)

**文档地址：**[https://DrissionPage.cn](https://DrissionPage.cn)

---

## ✅️ DataRecorder

是一个基于 python 的工具集，用于记录数据到文件。

使用方便，代码简洁，是一个可靠、省心且实用的工具。

**项目地址：**[https://gitee.com/g1879/DataRecorder](https://gitee.com/g1879/DataRecorder)

**文档地址：**[https://DrissionPage.cn/DataRecorderDocs](https://DrissionPage.cn/DataRecorderDocs/)

---

## ✅️ TimePinner

一个简单的计时工具。类似于代码中的秒表。

可标记多个点，以记录若干段时间长度。每段时间可以命名，以方便记忆，也可跳过无须记录的时间段。

**项目地址：**[https://gitee.com/g1879/TimePinner](https://gitee.com/g1879/TimePinner)

---

## ✅️ MixPage

DrissionPage 的早期版本，网页自动化工具。对 selenium 和 requests 进行了封装，可实现两者的同步。

在 DrissionPage 使用自研内核后，旧代码独立为一个库，以示纪念。

该库不再会有功能更新。

**项目地址：**[https://gitee.com/g1879/MixPage](https://gitee.com/g1879/MixPage)

**文档地址：**[https://DrissionPage.cn/MixPageDocs/](https://DrissionPage.cn/MixPageDocs/)

---

## ✅️ ListPage

优雅的列表爬虫。

本库是专门用于爬取或操作列表式网页的页面类，基于 MixPage。

页面类抽象了列表式页面基本特征，封装了常用方法。只需少量设置即可进行爬取或页面操作，实现可复用、可扩展。

广泛适用于各种网站的列表页面。

该库目前基于 MixPage，代码比较旧，以后有时间可能会改为基于新版 DrissionPage。

**项目地址：**[https://gitee.com/Drission/ListPage](https://gitee.com/Drission/ListPage)

---

## ✅️ FlowViewer

FlowViewer 是一个基于 Python 的 Chromium 内核浏览器数据包监听器。

它可以异步监听浏览器收发数据，实时返回结果供同步程序使用。

作为作者练手的项目，该库曾用于与 DrissionPage 搭配使用。

由于 DrissionPage 4.0 开始已内置了更好的监听器，此库现已不再更新。

**项目地址：**[https://gitee.com/Drission/FlowViewer](https://gitee.com/Drission/FlowViewer)