<script src="https://cdn.wwads.cn/js/makemoney.js" async></script>
<div class="wwads-cn wwads-horizontal" data-id="317"></div><br/>

## ✅️️ 创建`DownloadKit`对象

创建`DownloadKit`对象时，`driver`参数可传入一个用于携带登录信息的对象，可以是多种类型。

当传入的是`DrissionPage`的页面对象时，还可以根据页面访问的 url 自动设置`referer`参数。

|     初始化参数     |                                                     类型                                                     |    默认值     | 说明                                                                                         |
|:-------------:|:----------------------------------------------------------------------------------------------------------:|:----------:|--------------------------------------------------------------------------------------------|
|  `save_path`  |                                              `str`<br>`Path`                                               |   `None`   | 文件保存路径                                                                                     |
|    `roads`    |                                                   `int`                                                    |    `10`    | 可同时运行的线程数                                                                                  |
|   `driver`    | `Session`<br>`SessionOptions`<br>`ChromiumPage`<br>`WebPage`<br>`SessionPage`<br>`ChromiumTab`<br>`MixTab` |   `None`   | 用于提供下载连接信息的页面或链接对象                                                                         |
| `file_exists` |                                                   `str`                                                    | `'rename'` | 有同名文件名时的处理方式，可选`'skip'`, `'overwrite'`, `'rename'`, `'add'`（缩写：`'s'`, `'o'`, `'r'`, `'a'`） |

---

## ✅️️ 示例

### 📌 直接创建

```python
from DownloadKit import DownloadKit

d = DownloadKit()
```

---

### 📌 接收`Session`对象

```python
from requests import Session
from DownloadKit import DownloadKit

session = Session()
d = DownloadKit(driver=session)
```

---

### 📌 接收`SessionOptions`对象

```python
from DrissionPage import SessionOptions
from DownloadKit import DownloadKit

so = SessionOptions()
d = DownloadKit(driver=so)
```

---

### 📌 接收页面对象

```python
from DrissionPage import ChromiumPage
from DownloadKit import DownloadKit

p = ChromiumPage()
d = DownloadKit(driver=p)
```